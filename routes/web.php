<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome', ['channer'=>'Hello Bangladesh']);
});

Route::get('/mapis', function () {
    echo "this is age middleware";
})->name('map');

Route::get('/check', function () {
    return view('welcome');
})->name('check');



Route::get('/age', function () {
    echo "this is age middleware";
});

Route::get('/create', function () {
    return view('create');
})->name('create');


Route::get('/alldata', 'HelloController@index')->name('alldata')->middleware('verified');
Route::post('/store', 'HelloController@store')->name('store');
Route::get('/view/single/{id}', 'HelloController@show');
Route::get('/delete/single/{id}', 'HelloController@destroy');
Route::get('/edit/single/{id}', 'HelloController@edit');
Route::post('/update/single/{id}', 'HelloController@update');
Route::get('/form', 'HelloController@form')->middleware('verified');
Route::post('/form/store', 'HelloController@formstore')->name('form.store');
Route::get('/image', 'HelloController@image');
Route::get('/singleimage/{id}', 'HelloController@single');
Route::get('/image/delete/{id}', 'HelloController@DeletePost');
Route::get('/image/single/{id}', 'HelloController@ImageEdit');





Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');

Route::get('generate-pdf','HelloController@generatePDF');

Route::get('/export_excel/excel', 'HelloController@excel')->name('export_excel.excel');


