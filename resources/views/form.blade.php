<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Vertical (basic) form</h2>
   @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
  <form action="{{route('form.store')}}" enctype="multipart/form-data" method="post">
  	@csrf
    

    <div class="form-group">
       <label for="exampleFormControlSelect1">Example select</label>
	    <select class="form-control" name="name_id" id="exampleFormControlSelect1">
	      @foreach($data as $row)
	      <option value="{{$row->id}}">{{$row->name}}</option>
	      @endforeach
	    </select>
    </div>
    <div class="form-group">
     	<label for="exampleFormControlFile1">Images</label>
    	<input type="file" name="image" class="form-control-file" id="exampleFormControlFile1">
    </div>
    <div class="checkbox">
      <label><input type="checkbox" name="remember"> Remember me</label>
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
  </form>
</div>

</body>
</html>
