<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
  

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  

 
 
  
</head>
<body>

<div class="container">
  <h2>Striped Rows</h2>
  <p>The .table-striped class adds zebra-stripes to a table:</p>            
  <table class="table table-striped" id="myTable">
    <thead>
      <tr>
        <th>Id</th>
        <th>Firstname</th>
        
        <th>Image</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
     @foreach($data as $row)
      <tr>
        <td>{{$row->id}}</td>
        <td>{{$row->name}}</td>
        <td>
        	<img src="{{url($row->image)}}" width="300px" height="200px">
        </td>
        <td>
        	<a class="btn btn-success" href="{{url('/singleimage/'.$row->id)}}" role="button">View</a>
        	<a class="btn btn-info" href="{{url('/image/single/'.$row->id)}}" role="button">Edit</a>
        	<a class="btn btn-danger" href="{{url('/image/delete/'.$row->id)}}" role="button">Delete</a>

        </td>
       </tr>
      @endforeach
    </tbody>
  </table>
</div>

<script type="text/javascript">
	$(document).ready( function () {
    $('#myTable').DataTable();
} );
	
</script>


<script>
  @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
  @endif
</script>

</body>
</html>
