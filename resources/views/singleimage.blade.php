<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Hell {{$data->name}}</h2>
  <a class="btn btn-success" href="{{url('alldata')}}" role="button">All Data</a>
  <br>
  <ul class="list-group">
    <li class="list-group-item">{{$data->id}}</li>
    <li class="list-group-item">{{$data->name}}</li>
   <img src="{{url($data->image)}}" width="300px" height="200px">
  </ul>
</div>

</body>
</html>
