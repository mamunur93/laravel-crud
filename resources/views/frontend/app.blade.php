<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible">
		<meta name="viewport" content="">
    	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- CSRF Token -->
		<meta name="csrd-token" content="{{ csrf_token() }}">
		  
		  <!-- Favicon -->
    	<link rel="icon" href="{{ asset('assets/frontend/img/core-img/favicon.ico')}}">

		<title>@yield('title') - {{ config('app.name', 'Blog') }}</title>


	    <!-- Stylesheet -->
	    <link rel="stylesheet" href="{{ asset('assets/frontend/css/style.css') }}">

	    <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/css/bootstrap.min.css')}}">

	    <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/css/classy-nav.css')}}">

	    <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/css/font-awesome.min.css')}}">

	    <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/css/magnific-popup.css')}}"> 

	    <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/css/nice-select.css')}}"> 

	    <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/css/owl.carousel.min.css')}}">
	    
	    <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/css/animation.css')}}">
	    
	    <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/css/themify-icons.css')}}"> 
	    <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/scss/reboot.scss')}}">



	    @stack('css')

	</head>

	<body>
		@include('frontend.partials.header');
		 <!-- ##### Header Area End ##### -->

		@yield('content')
    	<!-- ##### Vizew Psot Area End ##### -->

    	<!-- ##### Footer Area Start ##### -->
    	@include('frontend.partials.footer');
    	   <!-- ##### Footer Area End ##### -->
    	@stack('js')
	    <!-- ##### All Javascript Script ##### -->
	    <!-- jQuery-2.2.4 js -->
	    <script src="{{ asset('assets/frontend/js/jquery/jquery-2.2.4.min.js')}}"></script>
	    <!-- Popper js -->
	    <script src="{{ asset('assets/frontend/js/bootstrap/popper.min.js')}}"></script>
	    <!-- Bootstrap js -->
	    <script src="{{ asset('assets/frontend/js/bootstrap/bootstrap.min.js')}}"></script>
	    <!-- All Plugins js -->
	    <script src="{{ asset('assets/frontend/js/plugins/plugins.js')}}"></script>
	    <!-- Active js -->
	    <script src="{{ asset('assets/frontend/js/active.js')}}"></script>

<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/95c75768/cloudflare-static/rocket-loader.min.js" data-cf-settings="db066e07ca7cc0d3d18acd60-|49" defer=""></script>

	</body>


</html>