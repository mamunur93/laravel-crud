<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Redirect;
use DB;
use PDF;
use Excel;
use App\Exports\CustomerData;

class HelloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return view('layouts.app');
       $data = DB::table('students')->get();
       return view('alldata',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function generatePDF()
    {
       $data = DB::table('students')->get();
        $pdf = PDF::loadView('myPDF', compact('data'));
  
        return $pdf->download('itsolutionstuff.pdf');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
        'name' => 'required|max:255',
        'email' => 'required',
    ]);
        $data=array();
        $data['name']=$request->name;
        $data['email']=$request->email;
        $catagory=DB::table('students')->insert($data);
          if ($catagory) {
            $notification = array(
                    'message' => 'I am a successful message!', 
                    'alert-type' => 'success'
                );
              return redirect()->route('alldata')->with($notification);
          }
          else{
            $notification = array(
                    'message' => 'Sorry! Data create failed', 
                    'alert-type' => 'error'
                );
              return redirect()->back('/')->with($notification);
          }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $cat=DB::table('students')->where('id',$id )->first();
       return view('single')->with('cat',$cat);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat=DB::table('students')->where('id',$id )->first();
       return view('edit')->with('cat',$cat);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
        'name' => 'required|max:255',
        'email' => 'required',
    ]);
        $data=array();
        $data['name']=$request->name;
        $data['email']=$request->email;
        // print_r($data);
        // die();
        $catagory=DB::table('students')->where('id',$id)->update($data);
          if ($catagory) {
            $notification = array(
                    'message' => 'I am a Update successful !', 
                    'alert-type' => 'success'
                );
              return redirect()->route('alldata')->with($notification);
          }
          else{
            $notification = array(
                    'message' => 'Nothis to update', 
                    'alert-type' => 'error'
                );
              return redirect()->route('alldata')->with($notification);
          }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $catagory=DB::table('students')->where('id',$id )->delete();
         if ($catagory) {
            $notification = array(
                    'message' => 'Data Delete successfully!', 
                    'alert-type' => 'success'
                );
              return redirect()->route('alldata')->with($notification);
          }
          else{
            $notification = array(
                    'message' => 'Sorry! Data Delete failed', 
                    'alert-type' => 'error'
                );
              return redirect()->back('/')->with($notification);
          }
    }

    public function form(){
        $data = DB::table('students')->get();
       return view('form',compact('data'));
    }
    public function formstore(Request $request){

         $validatedData = $request->validate([
       'image' => 'required|image|mimes:jpg,jpeg,png,gif,PNG',
       'name_id' => 'required',
    ]);
        $data=array();
        $data['cat_id']=$request->name_id;
        // $data['email']=$request->email;
        $image=$request->file('image');
        
        $image_name=hexdec(uniqid());
            $ext=strtolower($image->getClientOriginalExtension());
            $image_full_name=$image_name.'.'.$ext;
            $upload_path='public/frontend/image/';
            $image_url=$upload_path.$image_full_name;
            $success=$image->move($upload_path,$image_full_name);
            $data['image']=$image_url;
        
        $data['image'] =$image_url;
         $catagory=DB::table('images')->insert($data);
          if ($catagory) {
            $notification = array(
                    'message' => 'I am a Update successful !', 
                    'alert-type' => 'success'
                );
              return redirect()->route('image')->with($notification);
          }
          else{
            $notification = array(
                    'message' => 'Nothis to update', 
                    'alert-type' => 'error'
                );
              return redirect()->route('image')->with($notification);
          }
    }

    public function image(){
       //  $data = DB::table('images')->get();
       // return view('images',compact('data'));

        $data=DB::table('images')->join('students','images.cat_id','students.id')->select('images.*','students.name')
              ->get();
        return view('images',compact('data'));
    }

    public function single($id){
        // echo $id;
        // die();
         $data=DB::table('images')
         ->join('students','images.cat_id','students.id')
         ->select('images.*','students.name')->where('images.id', $id)
         ->first();
           return view('singleimage',compact('data'));

    }

    public function DeletePost($id)
    {
        $post=DB::table('images')->where('id',$id)->first();
        $image=$post->image;
        $delete=DB::table('images')->where('id',$id)->delete();
        if ($delete) {
            unlink($image);
            $notification=array(
                'messege'=>'Successfully Post Deleted !',
                'alert-type'=>'success'
                 );
             return Redirect()->back()->with($notification);
        }else{
            $notification=array(
                'messege'=>'Something went wrong !',
                'alert-type'=>'error'
                 );
             return Redirect()->back()->with($notification);
        }
    }

    public function ImageEdit($id){

        $data=DB::table('students')->get();
        $image=DB::table('images')->where('id',$id)->first();
        return view('editimage',compact('data','image'));

    }


     public function UpdatePost(Request $request,$id)
    {
         $validatedData = $request->validate([
       //'image' => 'image|mimes:jpg,jpeg,png,gif,PNG|max:2048',
       'name_id' => 'required',
    ]);

        $data=array();
         $data['cat_id']=$request->name_id;
       
        $image=$request->file('image');
        if ($image) {
            $image_name=hexdec(uniqid());
            $ext=strtolower($image->getClientOriginalExtension());
            $image_full_name=$image_name.'.'.$ext;
            $upload_path='public/frontend/image/';
            $image_url=$upload_path.$image_full_name;
            $success=$image->move($upload_path,$image_full_name);
            $data['image']=$image_url;
            unlink($request->old_photo);
            DB::table('images')->where('id',$id)->update($data);
             $notification=array(
                'messege'=>'Successfully Post Updated',
                'alert-type'=>'success'
                 );
             return Redirect()->route('all.post')->with($notification);
        }else{
             $data['image']=$request->old_photo;
             DB::table('images')->where('id',$id)->update($data);
             $notification=array(
                'messege'=>'Successfully Post Updated',
                'alert-type'=>'success'
                 );
             return Redirect()->url('image')->with($notification);
        }
    }


    public function excel(){
        
        return Excel::download(new CustomerData, 'users.xlsx');
    }

   
}
